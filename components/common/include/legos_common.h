//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_COMMON_H_
#define _LEGOS_COMMON_H_

#include <string.h>
#include <math.h>
#include <time.h>

// Timer settings
#define TIMER_DIVIDER 50000
#define TIMER_SCALE (TIMER_BASE_CLK / TIMER_DIVIDER)

// WiFi Settings
#define WIFI_STA_SSID "LEGOS"
#define WIFI_STA_PASS "legos#wifi"

// MQTT Settings
#define MQTT_USER "LEGOS"
#define MQTT_PASS "legos#mqtt"
#define MQTT_HOST "mqtt://192.168.137.1"
#define MQTT_PORT 1883
#define MQTT_RATE 1

#define	MQTT_QOS_LEVEL_0 0	// At most once
#define	MQTT_QOS_LEVEL_1 1	// At least once
#define	MQTT_QOS_LEVEL_2 2	// Exactly once

#define	MQTT_ENCODE_UL20 (0)
#define	MQTT_ENCODE_JSON (1)
#define MQTT_ENCODE MQTT_ENCODE_UL20

/**
 * @brief Communication service topic
 */
static const char *mqtt_topic_service = "/legos/service/cmd";

/**
 * @brief MQTT payload structure
 */
typedef struct
{
	char key[64];
	char val[64];
	char str[128];
} mqtt_payload_t;

uint16_t mqtt_next_field(char* str, mqtt_payload_t* payload, uint16_t offset);
void mqtt_marshall(char* key, float value, mqtt_payload_t *payload);
int  mqtt_unmarshall(char* str, char* key, mqtt_payload_t *payload);


// LED intensity presets
typedef enum {
    LED_INTENSITY_0 = 0,
    LED_INTENSITY_10 = 819,
    LED_INTENSITY_20 = 1638,
    LED_INTENSITY_30 = 2457,
    LED_INTENSITY_40 = 3276,
    LED_INTENSITY_50 = 4096,
    LED_INTENSITY_60 = 4915,
    LED_INTENSITY_70 = 5734,
    LED_INTENSITY_80 = 6553,
    LED_INTENSITY_90 = 7372,
    LED_INTENSITY_100 = 8191
} led_duty_t;


#endif //_LEGOS_COMMON_H_