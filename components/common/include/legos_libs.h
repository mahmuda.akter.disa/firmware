//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_LIBS_H_
#define _LEGOS_LIBS_H_

/**
 * @brief Compilation target
 *        Select the proper compilation target by replacing
 *        ENTITY with the correct entity name
 */
#define ENTITY

#ifdef BRANCH
    #include "branch.h"
#elif defined DATACENTER
    #include "data_center.h"
#elif defined ECSTATION
  #include "ec_station.h"
#elif defined FACTORY
  #include "factory.h"
#elif defined HOSPITAL
  #include "hospital.h"
#elif defined HOUSE
  #include "house.h"
#elif defined POWERPLANT
  #include "power_plant.h"
#elif defined SKYSCRAPER
  #include "skyscraper.h"
#elif defined SOLARFARM
  #include "solar_farm.h"
#elif defined STADIUM
  #include "stadium.h"
#elif defined SUBSTATION
  #include "substation.h"
#elif defined SUPERMARKET
  #include "supermarket.h"
#elif defined WINDFARM
  #include "wind_farm.h"
#endif

#endif //_LEGOS_LIBS_H_