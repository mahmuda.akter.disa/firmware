//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_MQTT_H_
#define _LEGOS_MQTT_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/periph_ctrl.h"
#include "driver/timer.h"

#include "legos_libs.h"
#include "legos_common.h"


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t init_mqtt();

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_MQTT_H_