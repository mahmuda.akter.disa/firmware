//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_common.h"
#include "esp_log.h"
#include "stdio.h"


/**
 * @brief Next MQTT field
 *        Iterative scan of MQTT payload fields
 *
 * @param[in] ch MQTT message
 * @param[in,out] payload MQTT payload
 * @param[in] offset MQTT message offset
 * 
 * @return scan offset
 */
uint16_t mqtt_next_field(char* str, mqtt_payload_t* payload, uint16_t offset)
{
	uint16_t end = 0;
	uint16_t pos[4] = { 0,0,0,0 };
	int cnt = 0;

	switch (MQTT_ENCODE)
	{
	case MQTT_ENCODE_JSON:
		for (int i = offset; i <= strlen(str); i++) {
			if (str[i] == '\"')
				pos[cnt++] = i;
			if ((str[i] == ';') || (str[i] == '}')) {
				end = i;
				break;
			}
		}

		if (cnt < 4) {
			end = strlen(str);
			break;
		}

		memcpy(payload->key, (void*)&str[pos[0] + 1], pos[1] - pos[0] - 1);
		memcpy(payload->val, (void*)&str[pos[2] + 1], pos[3] - pos[2] - 1);
		payload->key[pos[1] - pos[0] - 1] = '\0';
		payload->val[pos[3] - pos[2] - 1] = '\0';
		ESP_LOGD("Mqtt", "JSON pair:\t%s:%s", payload->key, payload->val);
		break;

	case MQTT_ENCODE_UL20:
		for (int i = offset; i <= strlen(str); i++) {
			end = i;
			if ((str[i] == '|') || (str[i] == '\0'))
				pos[cnt++] = i;
			if (cnt == 2)
				break;
		}

		if (cnt < 2) {
			end = strlen(str);
			break;
		}

		memcpy(payload->key, (void*)&str[offset], pos[0] - offset);
		memcpy(payload->val, (void*)&str[pos[0] + 1], pos[1] - pos[0] - 1);
		payload->key[pos[0] - offset] = '\0';
		payload->val[pos[1] - pos[0] - 1] = '\0';
		ESP_LOGD("Mqtt", "UL pair:\t%s:%s", payload->key, payload->val);
		break;

	default:
		break;
	}

	offset = end + 1;
	return offset;
}


/**
 * @brief MQTT payload marshall
 *        Build MQTT message using key-value pairs
 *
 * @param[in] key MQTT message key
 * @param[in] value MQTT message value
 * @param[in,out] payload MQTT payload
 * 
 * @return scan offset
 */
void mqtt_marshall(char* key, float value, mqtt_payload_t *payload)
{
	memset((void*)payload, 0, sizeof(mqtt_payload_t));

	switch (MQTT_ENCODE)
	{
	case MQTT_ENCODE_JSON:
		sprintf(payload->str, "{\"%s\":\"%.3f\"}", key, value);
		break;
	case MQTT_ENCODE_UL20:
		sprintf(payload->str, "%s|%.3f", key, value);
		break;
	default:
		break;
	}
	ESP_LOGD("Mqtt", "Marshall:\t%s=%.3f", key, value);
}


/**
 * @brief MQTT payload unmarshall
 *        Split MQTT message using key-value pairs
 *
 * @param[in] str MQTT message
 * @param[in] key MQTT message key
 * @param[in,out] payload MQTT payload
 * 
 * @return scan offset
 */
int mqtt_unmarshall(char* str, char* key, mqtt_payload_t *payload)
{
	uint16_t offset = 0;

	do {
		offset = mqtt_next_field(str, payload, offset);
		if (strcmp(payload->key, key) == 0){
			ESP_LOGD("Mqtt", "Unmarshall:\t%s=%s", payload->key, payload->val);
			break;
		}
		memset(payload->key, 0, 64);
		memset(payload->val, 0, 64);
	} while (offset < strlen(str));

	return (strlen(payload->val));
}