//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_wifi.h"
#include "esp_log.h"
#include "esp_http_server.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define NETW_CONNECT_BIT BIT0
#define NETW_FAIL_BIT      BIT1

static EventGroupHandle_t netif_event_group;
static esp_netif_ip_info_t ip_info;
static httpd_handle_t server = NULL;
static esp_netif_t * sta_netif = NULL;
static wifi_config_t wifi_config;

static const char *TAG = "Network";


/**
 * @brief HTTP GET handler
 *        Called upon HTTP request GET
 * 
 * @param[in] req Pointer to http request
 * 
 * @return void
 */
esp_err_t server_handler_get(httpd_req_t* req)
{
	ESP_LOGD(TAG, "HTTP GET handler");

	char* buf;
	size_t buf_len;

	/* Get header value string length and allocate memory for length + 1,
	 * extra byte for null termination */
	buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
	if (buf_len > 1) {
		buf = (char*)malloc(buf_len);
		/* Copy null terminated value string into buffer */
		if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK)
			ESP_LOGD(TAG, "Found header => Host: %s", buf);
		free(buf);
	}

	buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-2") + 1;
	if (buf_len > 1) {
		buf = (char*)malloc(buf_len);
		if (httpd_req_get_hdr_value_str(req, "Test-Header-2", buf, buf_len) == ESP_OK)
			ESP_LOGD(TAG, "Found header => Test-Header-2: %s", buf);
		free(buf);
	}

	buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-1") + 1;
	if (buf_len > 1) {
		buf = (char*)malloc(buf_len);
		if (httpd_req_get_hdr_value_str(req, "Test-Header-1", buf, buf_len) == ESP_OK)
			ESP_LOGD(TAG, "Found header => Test-Header-1: %s", buf);
		free(buf);
	}

	/* Read URL query string length and allocate memory for length + 1,
	 * extra byte for null termination */
	buf_len = httpd_req_get_url_query_len(req) + 1;
	if (buf_len > 1) {
		buf = (char*)malloc(buf_len);
		if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
			ESP_LOGD(TAG, "Found URL query => %s", buf);

			char param[32];
			/* Get value of expected key from query string */
			if (httpd_query_key_value(buf, "query1", param, sizeof(param)) == ESP_OK) {
				ESP_LOGD(TAG, "Found URL query parameter => query1=%s", buf);
			}
			if (httpd_query_key_value(buf, "query2", param, sizeof(param)) == ESP_OK) {
				ESP_LOGD(TAG, "Found URL query parameter => query2=%s", buf);
			}
			if (httpd_query_key_value(buf, "query3", param, sizeof(param)) == ESP_OK) {
				ESP_LOGD(TAG, "Found URL query parameter => query3=%s", buf);
			}
		}
		free(buf);
	}

	/* Set some custom headers */
	httpd_resp_set_hdr(req, "Custom-Header-1", "Custom-Value-1");
	httpd_resp_set_hdr(req, "Custom-Header-2", "Custom-Value-2");

	/* Send response with custom headers and body set as the
	 * string passed in user context*/
	char* HTML = (char*)malloc(10000);
	http_post(HTML);
	httpd_resp_send(req, (const char*)HTML, strlen(HTML));
	free(HTML);

	/* After sending the HTTP response the old HTTP request
	 * headers are lost. Check if HTTP request headers can be read now. */
	if (httpd_req_get_hdr_value_len(req, "Host") == 0)
		ESP_LOGD(TAG, "Request headers lost");

	return ESP_OK;
}


/**
 * @brief HTTP GET handler
 *        Called upon HTTP request GET
 * 
 * @param[in] req Pointer to http request
 * 
 * @return void
 */
esp_err_t server_handler_post(httpd_req_t* req)
{
	ESP_LOGD(TAG, "HTTP POST handler");

	char buf[1500];
	int ret = 0;
	int remaining = req->content_len;

	while (remaining > 0) {
		/* Read the data for the request */
		if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0) {
			if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
				/* Retry receiving if timeout occurred */
				continue;
			}
			return ESP_FAIL;
		}
		remaining -= ret;
	}

	/* Log data received */
	ESP_LOGD(TAG, "Data received: %d bytes", ret);
	ESP_LOGD(TAG, "Buffer:\n%.*s\n", ret, buf);

	if (http_read(buf) == ESP_OK)
		ESP_LOGD(TAG, "Request interpreted succesfully");

	// Send updated html page
	char* HTML = (char*)malloc(10000);
	http_post(HTML);
	httpd_resp_send(req, (const char*)HTML, strlen(HTML));
	free(HTML);

	return ESP_OK;
}

static httpd_uri_t uri_get  = { "/", HTTP_GET, server_handler_get,  NULL };
static httpd_uri_t uri_post = { "/",HTTP_POST, server_handler_post, NULL };


/**
 * @brief Start HTTP server
 * 
 * @return httpd_handle_t
 */
httpd_handle_t server_start()
{
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	esp_netif_get_ip_info(sta_netif, &ip_info);
	ESP_LOGD(TAG, "Starting HTTP local server on " IPSTR ":%d", IP2STR(&ip_info.ip), config.server_port);

	// Start the httpd server	
	if (httpd_start(&server, &config) == ESP_OK) {
		// Set URI handlers
		ESP_LOGD(TAG, "Registering URI handlers");
		httpd_register_uri_handler(server, &uri_get);
		httpd_register_uri_handler(server, &uri_post);
		return server;
	}

	ESP_LOGE(TAG, "Could not start server");
	return NULL;
}


/**
 * @brief Stop HTTP server
 * 
 * @return
 *  - ESP_OK : Server stopped successfully
 *  - ESP_ERR_INVALID_ARG : Handle argument is Null
 */
esp_err_t server_stop()
{
	ESP_LOGD(TAG, "Halting HTTP server");
	// Stop the httpd server
	return httpd_stop(server);
}


/**
 * @brief WiFi event handler
 *        Finite State Machine
 * 
 * @param[in] arg Parameters passed to the task
 * @param[in] event_base Event primitive
 * @param[in] event_id Event id
 * @param[in] event_data Event data
 * 
 * @return void
 */
void network_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
	ESP_LOGD(TAG, "Netw EVENT");
	if (event_base == WIFI_EVENT) {
		switch (event_id) {
		case WIFI_EVENT_STA_START:
			ESP_LOGD(TAG, "WiFi Event:\tSTA start");
			esp_wifi_connect();
			break;

		case WIFI_EVENT_STA_CONNECTED:
			ESP_LOGD(TAG, "WiFi Event:\tSTA connected");
			break;

		case WIFI_EVENT_STA_DISCONNECTED:
			ESP_LOGD(TAG, "WiFi Event:\tSTA disconnected");
			esp_wifi_connect();
			xEventGroupClearBits(netif_event_group, NETW_CONNECT_BIT);
			wifi_event_sta_disconnected_t* wifi_event = (wifi_event_sta_disconnected_t*) event_data;
			if (wifi_event->reason == WIFI_REASON_NO_AP_FOUND) {
				// ToDo
			}
			break;

		default:
			break;
		}
	}
	else if(event_base == IP_EVENT) {
		switch (event_id) {
		case IP_EVENT_STA_GOT_IP:{
			esp_netif_dns_info_t dns_info;
			ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
			esp_netif_t *netif = event->esp_netif;

			ESP_LOGD(TAG, "IP Event:\tSTA Connect to Server ");
			ESP_LOGD(TAG, "IP          : " IPSTR, IP2STR(&event->ip_info.ip));
			ESP_LOGD(TAG, "Netmask     : " IPSTR, IP2STR(&event->ip_info.netmask));
			ESP_LOGD(TAG, "Gateway     : " IPSTR, IP2STR(&event->ip_info.ip));
			esp_netif_get_dns_info(netif, 0, &dns_info);
			ESP_LOGD(TAG, "Name Server1: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));
			esp_netif_get_dns_info(netif, 1, &dns_info);
			ESP_LOGD(TAG, "Name Server2: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));

			xEventGroupSetBits(netif_event_group, NETW_CONNECT_BIT);
			}
			break;

		default:
			break;
		}
	}
}

/**
 * @brief Init STA
 *        Initialize the WiFi STA interface
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_STA()
{
	ESP_LOGD(TAG, "Init STA mode");

	wifi_sta_config_t sta = {
		.ssid = WIFI_STA_SSID,
		.password = WIFI_STA_PASS
	};
	wifi_config.sta = sta;

	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_LOGD(TAG, "STA mode credentials: SSID:[%s] password:[%s]", sta.ssid, sta.password);

	return esp_wifi_start();
}


/**
 * @brief Init WiFi
 *        Initialize the WiFi network adapter
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_wifi()
{
	ESP_LOGD(TAG, "Init NVS Flash");
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

	ESP_LOGD(TAG, "Init Network Interface");

	netif_event_group = xEventGroupCreate();
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

    sta_netif = esp_netif_create_default_wifi_sta();
	assert(sta_netif);

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));

	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));


	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(init_STA());

	xEventGroupWaitBits(netif_event_group, NETW_CONNECT_BIT | NETW_FAIL_BIT, pdFALSE, pdFALSE, portMAX_DELAY);

	return ESP_OK;
}
