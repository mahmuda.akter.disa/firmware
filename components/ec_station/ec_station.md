# Electric Car station
The Electric Car Station provides a charging point for electric vehicles. The power absorbed from the grid is determined by the charging pattern, stored in a RFID tag inside the car.

## Main code
### Status structure
```c
struct ECStation
 {
    uint8_t     id[8];              // Unique port identifier
    uint8_t     charging_load;      // Charging load
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> telemetry[Telemetry]
    telemetry --> rfid[RFID]
    rfid --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> tag[Detect Car RFID]
    tag --> load[Update Workload]
    load --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/ec_station**
    - Telemetry
        - *Format*: `ECstation: <I> mA [<V> V]   Load: <val> %`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter. Current workload is **val**<br/>(*eg.* `ECstation: 121.7 mA [3.36 V]   Load: 75.1 %`)

### Subscribe
- **/legos/ec_station/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|ECstation`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
## HTTP callbacks

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/ec_station/ec_station.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/ec_station/ec_station.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/ec_station/ec_station.md)