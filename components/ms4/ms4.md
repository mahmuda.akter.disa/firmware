# AmbiMate Sensor Module - MS4 Series
## Description
This component provides an ESP32 library for interfacing the modules of the MS4 series. This series offers a four core sensor solution for Motion / Light / Temperature / Humidity, expandable to include  VOC  (Volatile  Organic  Compound), eCO2 (equivalent carbon dioxide ) and sound detection.

## Main code
### Status structure
```c
typedef struct
{
    uint8_t  status;                // MS4 event
    float    temp;                  // 5 °C to + 50 °C + 1 °C Accuracy, 1 second acquisition rate
    float    hum;                   // 5% to 95% RH, 3% accuracy, 1 second acquisition rate
    uint16_t light;                 // 0-1024 Lux, 1 second acquisition rate 
    float    batt_v;
    uint16_t co2_ppm;               // 400-8192ppm, 60 second acquisition rate, (an equivalent eCO2 measurement based on total VOC concentrations)
    uint16_t voc_ppb;               // 0-1187ppb, 60 second acquisition rate
} ms4_t;
```

### Methods
```c
// Read all sensors value
void ms4_readAll();

// Init the Ambimate multi-sensor
esp_err_t ms4_init(i2c_port_t v_i2c_num, uint8_t addr, ms4_t *data);
```

## Documents
Datasheet: [1-1773935](https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1-1773935-7_AmbiMate_MS4_Series%7F1908%7Fpdf%7FEnglish%7FENG_DS_1-1773935-7_AmbiMate_MS4_Series_1908.pdf%7F2316852-1)

Application: [114-133092](https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Specification+Or+Standard%7F114-133092%7FB%7Fpdf%7FEnglish%7FENG_SS_114-133092_B.pdf%7FN-A)/[114-133115](https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Specification+Or+Standard%7F114-133115%7F1%7Fpdf%7FEnglish%7FENG_SS_114-133115_1.pdf%7FN-A)

Specifications: [108-133092](https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Specification+Or+Standard%7F108-133092%7F1%7Fpdf%7FEnglish%7FENG_SS_108-133092_1.pdf%7FN-A)