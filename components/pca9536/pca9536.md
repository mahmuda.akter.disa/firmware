# PCA9536 IO Expander
## Description
This component provides an ESP32 library for interfacing the PCA9536 I/O expander by Texas Instruments. It provides general-purpose remote I/O expansion for most microcontroller families via the I2C interface [serial clock (SCL), serial data (SDA)].

## Main code
### Methods
```c
// Set PCA9536 channel level
void pca9536_set_gpio_level(uint8_t ch, uint8_t level);

// Get PCA9536 channel level
void pca9536_get_gpio_level(uint8_t ch, uint8_t *level);

// Init PCA9536 IO Expander
esp_err_t pca9536_init(i2c_port_t v_i2c_num, uint8_t addr);
```

## Documents
Datasheet: [PCA9536](https://www.ti.com/lit/ds/symlink/pca9536.pdf)
