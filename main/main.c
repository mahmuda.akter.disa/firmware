//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "legos_libs.h"
#include "legos_wifi.h"
#include "legos_mqtt.h"

static const char *TAG = "LEGOS";

void app_main(void)
{

	ESP_LOGI(TAG, "Startup..");
	ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
	ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

	ESP_LOGI(TAG, "Configuring Log levels");
	esp_log_level_set("*", ESP_LOG_INFO);


	ESP_LOGI(TAG, "Init modules..");
	init_entity();									// Init entity profile
	init_wifi();									// Init WIFI network interface
	init_mqtt();									// Init MQTT message protocol

    while(1){
		vTaskDelay(100 / portTICK_PERIOD_MS);		// Yield task
	}
}